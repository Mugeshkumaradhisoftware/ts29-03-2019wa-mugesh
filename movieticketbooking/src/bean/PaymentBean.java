package bean;

public class PaymentBean {
	String seats;
	 String email;
	 String Price;
	 String showid;
	 String total;
	 
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getShowid() {
		return showid;
	}
	public void setShowid(String showid) {
		this.showid = showid;
	}
	public String getSeats() {
	return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPrice() {
		return Price;
	}
	public void setPrice(String price) {
		Price = price;
	}
			 
}
