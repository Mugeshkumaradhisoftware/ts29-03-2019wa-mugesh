package controllerDB;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.PaymentBean;
import dao.PaymentDAO;

 
@WebServlet("/Payment")
public class Payment extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		String seats=request.getParameter("seats");
		 String email=request.getParameter("email");
		 String price=request.getParameter("price");
		 String showid=request.getParameter("showid");
		 String total=request.getParameter("total");
		 
		 String filename="Qrcodefornow";
		 
		 System.out.println(email);
		 
		 QrCode.GenerateQrCode(filename,seats,showid,total,price);
		 Mailsend.TicketBooking(email,seats,showid,total,price);
		 PaymentBean paynow=new PaymentBean();
		
		 paynow.setShowid(showid);
		 paynow.setEmail(email);
		 paynow.setPrice(price);
		 paynow.setSeats(seats);
		 paynow.setTotal(total);
		 
		 PaymentDAO paydao=new PaymentDAO();
		 paydao.Pay(paynow);
 
		 request.getRequestDispatcher("/Eticket.jsp").forward(request, response);
		 
		 
 		 
		 
		 
	}

	 
}
